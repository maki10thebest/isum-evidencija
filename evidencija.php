<?php
require_once 'navBar.php';
// Pokreni sesiju
session_start();
navBarUser();
if(!isset($_SESSION['USERNAME']) || empty($_SESSION['USERNAME'])){
    header("location: login.php");
    exit;
}

?>


<!DOCTYPE html>
<html>
<head>
    <!-- BOOTSTRAP-->
    <title>EVIDENCIJA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .form-control{
            width: 50%;
        }
        /*.all {
            width: 50%;
            margin: 0 auto;
            margin-top: 55px;
        }*/
        .pagination a{
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration:none;
            transition:background-color .3s;
        }
        .pagination a.active{
            background-color: #337ab7;;
            color: #fff;
        }

        .pagination {
            margin-top: 30px;

        }
        .pagination a:hover:not(.active){
            background-color: #ddd;
        }

    </style>
</head>
<body>
<div class="all">
<!--ukljucuje CRUD-->
<?php require_once 'process_evidencija.php'; ?>

<?php
//PORUKA NAKON AKCIJA
if (isset($_SESSION['message'])): ?>

    <div class="alert alert-<?=$_SESSION['msg_type']?>">
        <?php
        echo $_SESSION['message'];
        unset ($_SESSION['message']);
        ?>
    </div>
<?php endif ?>
<!--Povezuje se na bazu-->
<div class="container">
    <?php
    $mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));

    //define how many results you want per page
    $results_per_page = 10;

    // find out the number of results stored in databse
    $result = $mysqli->query("SELECT evidencija.IDNEDELJA, evidencija.VREMEDOLASKA, evidencija.VREMEODLASKA, zaposleni.IME, zaposleni.PREZIME FROM evidencija INNER JOIN zaposleni ON evidencija.IDZAPOSLENI = zaposleni.IDZAPOSLENI") or die($mysqli->error);
    $number_of_results = mysqli_num_rows($result);

    //determine number of total pages avilable
    $number_of_pages = ceil($number_of_results/$results_per_page);

    // determine which page number visitor is currently on
    if (!isset($_GET['page'])) {
        $page =1;
    } else {
        $page= $_GET['page'];
    }

    //determine the sql LIMIT starting number for the results on the displaying page
    $this_page_first_result = ($page-1)*$results_per_page;

    //display the links to the pages

    ?>

    <!--STAMPA-->
    <div class="row justify-content-center">
        <table class="table">
            <thead>
            <tr>
                <th>Zaposleni</th>
                <th>Nedelja</th>
                <th>Vreme dolaska</th>
                <th>Vreme odlaska</th>
                <th>Provedeno vreme</th>
                <th colspan="2"> Akcija</th>
            </tr>
            </thead>


            <?php
            // retrieve selected results from databse and displauy them on page
            $result = $mysqli->query("SELECT nedelja.RBNEDELJE, evidencija.VREMEDOLASKA, evidencija.VREMEODLASKA, zaposleni.IME, zaposleni.PREZIME
            FROM evidencija INNER JOIN zaposleni ON evidencija.IDZAPOSLENI = zaposleni.IDZAPOSLENI 
            INNER JOIN nedelja ON evidencija.IDNEDELJA = nedelja.IDNEDELJA
            LIMIT " . $this_page_first_result . ',' .$results_per_page) or die($mysqli->error);
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['IME']; echo ' '; echo $row['PREZIME']; ?></td>
                    <td><?php echo $row['RBNEDELJE']; ?></td>
                    <td><?php echo $row['VREMEDOLASKA']; ?></td>
                    <td><?php echo $row['VREMEODLASKA']; ?></td>
                    <?php $start_date = new DateTime($row['VREMEDOLASKA']);
                    $since_start = $start_date->diff(new DateTime($row['VREMEODLASKA']));?>
                    <td><?php echo $since_start->h.' hours';echo ' '; echo $since_start->i.' minutes<br>';?></td>
                    <td>
                        <a href="evidencija.php?edit=<?php echo $row['IDEVIDENCIJA']; ?>"
                           class="btn btn-info">Edit</a>
                        <a href="process_evidencija.php?delete=<?php echo $row['IDEVIDENCIJA'] ?>"
                           class="btn btn-danger">Delete</a>
                    </td>
                    <?php endwhile; ?>

                </tr>

        </table>
    </div>

    <?php
    function pre_r($array) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }
    echo "<div class='pagination'>";
   // for ($page=1;$page<=$number_of_pages;$page++) {
     for($i=1;$i<=$number_of_pages;$i++)   {
        if($i==$page)
            echo'<a class="active">' . $i . '</a>';
        else
            echo <<<'TAG'
<a href=" http://localhost/ISUMEvidencija/ISUM%20evidencija/evidencija.php?page=
TAG
 .$i . '">' .$i. '</a>';
        //echo '<a class="active" href="evidencija.php?page=' . $page . '" >' .$page.'</a>';
    }
    echo "</div>";
    ?>

    <!--KREIRA NOVOG ZAPOSLENOG-->
    <div class="row justify-content-center">
        <form action="process_evidencija.php" method="POST">
            <input type="hidden" name="IDEVIDENCIJA" value="<?php echo $id; ?>">
            <div class="form-group">
                <label>ZAPOSLENI</label>
                <select name="ime" class="form-control">
                    <?php   $result2 = $mysqli->query("SELECT * FROM zaposleni") or die($mysqli->error);
                    while ($row2 = $result2->fetch_assoc()): ?>
                        <option value="<?php echo $row2['IDZAPOSLENI']; ?>"><?php echo $row2['IME']; echo ' '; echo $row2['PREZIME']; ?></option>
            <?php endwhile; ?>
                </select>
            </div>
            <div class="form-group">
                <label>NEDELJA</label>
                <select name="nedelja" class="form-control">
                <?php   $result3 = $mysqli->query("SELECT * FROM nedelja") or die($mysqli->error);
                 while ($row3 = $result3->fetch_assoc()): ?>
                    <option value="<?php echo $row3['IDNEDELJA']; ?>"><?php echo $row3['RBNEDELJE']; ?></option>
            <?php endwhile; ?>
                </select>
            </div>
            <div class="form-group">
                <label>VREME DOLASKA</label>
                <input type="datetime-local" name="od" class="form-control" value="<?php echo $od; ?>" placeholder="Vreme dolaska">
            </div>
            <div class="form-group">
                <label>VREME ODLASKA</label>
                <input type="datetime-local" name="do" class="form-control" value="<?php echo $do; ?>" placeholder="Vreme odlaska">
            </div>
            <div class="form-group">
                <?php
                if($update == true):
                    ?>
                    <button type="submit" class="btn btn-info" name="update">Update</button>
                <?php else: ?>
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
                <?php endif; ?>
            </div>
        </form>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</div>
</body>

</html>