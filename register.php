
<?php

$mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));

$username = $password = $confirm_password = $name = $last = $email = "";
$username_err = $password_err = $confirm_password_err = $name_err = $last_err = $email_err = "";

//preuzimamo upisane varijable

if($_SERVER["REQUEST_METHOD"] == "POST"){

    //provera username-a

    if(empty(trim($_POST["username"]))){
        $username_err = "Molimo vas unesite username.";
    } else{
        $sql = "SELECT IDUSER FROM user WHERE USERNAME = ?";

        if($stmt = mysqli_prepare($mysqli, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_username);


            $param_username = trim($_POST["username"]);

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "Username vec postoji u sistemu.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Greska! Molimo vas pokusajte kasnije ponovo.";
            }
        }

        mysqli_stmt_close($stmt);
    }

    //provera pasworda

    if(empty(trim($_POST['password']))){
        $password_err = "Molimo vas unesite password";
    } elseif(strlen(trim($_POST['password'])) < 6){
        $password_err = "Password mora imati najmanje 6 karaktera.";
    } else{
        $password = trim($_POST['password']);
    }

    //confirm your password, mora da se slaze sa prethodnom varijablom

    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = 'Molimo vas potrvrdite password.';
    } else{
        $confirm_password = trim($_POST['confirm_password']);
        if($password != $confirm_password){
            $confirm_password_err = 'Uneti password-i se ne poklapaju.';
        }
    }

    //provera Imena

    if(empty(trim($_POST['name']))){
        $name_err = "Molimo vas unesite password";
    } elseif (!preg_match("/^[a-zA-Z]*$/", $name)) {
        $name_err = "Molimo vas unesite ime ponovo.";
    }
    else {
        $name = trim($_POST['name']);
    }

    //provera prezimena

    if(empty(trim($_POST['last']))){
        $last_err = "Molimo vas unesite prezime";
    }  elseif (!preg_match("/^[a-zA-Z]*$/", $last)){
        $last_err="Molimo vas unesite prezime ponovo";
    } else {
        $last = trim($_POST['last']);
    }

    //provera emaila

    if(empty(trim($_POST['email']))){
        $email_err = "Molimo vas unesite email";
    } //elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //$email_err= "Molimo vas unesite trrrr";
    //}
    else {
        $email = trim($_POST['email']);
    }

    //upis proverenih varijabla u tabelu

    if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($name_err) && empty($last_err) && empty($email_err) ){

        $sql = "INSERT INTO user (USERNAME, PASSWORD, EMAIL, NAME, LAST) VALUES (?, ?, ?, ?, ?)";

        if($stmt = mysqli_prepare($mysqli, $sql)){
            mysqli_stmt_bind_param($stmt, "sssss", $param_username, $param_password, $param_email, $param_name, $param_last);

            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);
            $param_email = $email;
            $param_name = $name;
            $param_last = $last;


            if(mysqli_stmt_execute($stmt)){
                header("location: login.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }

        mysqli_stmt_close($stmt);
    }

    mysqli_close($mysqli);
}
?>

<!DOCTYPE html>
<html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!--<script type="stylesheet" src="style.css"></script>-->
<head>
    <style type="text/css">

        @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
        .login-block{
            background: #DE6262;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to bottom, #FFB88C, #A70532);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to bottom, #FFB88C, #A70532); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            float:left;
            width:100%;
            padding : 50px 0;
        }
        .banner-sec{background:url(https://static.pexels.com/photos/33972/pexels-photo.jpg)  no-repeat left bottom; background-size:cover; min-height:500px; border-radius: 0 10px 10px 0; padding:0;}
        .container{background:#fff; border-radius: 10px; box-shadow:15px 20px 0px rgba(0,0,0,0.1);}
        .carousel-inner{border-radius:0 10px 10px 0;}
        .carousel-caption{text-align:left; left:5%;}
        .login-sec{padding: 50px 30px; position:relative;}
        .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
        .login-sec .copy-text i{color:#FEB58A;}
        .login-sec .copy-text a{color:#E36262;}
        .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #DE6262;}
        .login-sec h2:after{content:" "; width:100px; height:5px; background:#FEB58A; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
        .btn-login{background: #A70532; color:#fff; font-weight:600;}
        .banner-text{width:70%; position:absolute; bottom:40px; padding-left:20px;}
        .banner-text h2{color:#fff; font-weight:600;}
        .banner-text h2:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;}
        .banner-text p{color:#fff;}
    </style>
</head>
<body>
<section class="login-block">
    <div class="container">
        <div class="row">
            <div class="col-md-4 login-sec">
                <h2 class="text-center">Sign Up</h2>
                <p>Molimo vas popunite sva polja kako bi ste napravili novi nalog.</p>
                <form  class="login-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                        <label for="exampleInputEmail1" class="text-uppercase">Name</label>
                        <input type="text" name="name"class="form-control" value="<?php echo $name; ?>">
                        <span class="help-block"><?php echo $name_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($last_err)) ? 'has-error' : ''; ?>">
                        <label for="exampleInputEmail1" class="text-uppercase">Last</label>
                        <input type="text" name="last"class="form-control" value="<?php echo $last; ?>">
                        <span class="help-block"><?php echo $last_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                        <label for="exampleInputEmail1" class="text-uppercase" >email</label>
                        <input type="text" name="email"class="form-control" value="<?php echo $email; ?>">
                        <span class="help-block"><?php echo $email_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                        <label for="exampleInputEmail1" class="text-uppercase" >Username</label>
                        <input type="text" name="username"class="form-control" value="<?php echo $username; ?>">
                        <span class="help-block"><?php echo $username_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                        <label for="exampleInputEmail1" class="text-uppercase" >Password</label>
                        <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                        <span class="help-block"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                        <label for="exampleInputEmail1" class="text-uppercase" >Potvrda Password-a</label>
                        <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                        <span class="help-block"><?php echo $confirm_password_err; ?></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-login float-left" value="Sacuvaj">
                        <input type="reset" class="btn btn-default" value="Resetuj">
                    </div>
                    <p>Vec imate nalog? <a href="login.php">Ulogujte se ovde</a>.</p>
                </form>
            </div>
</body>
</html>
