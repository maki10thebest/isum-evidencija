<?php ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.search-box input[type="text"]').on("keyup input", function () {
                var inputVal = $(this).val();
                var resultDropdown = $(this).siblings(".result");
                if (inputVal.length) {
                    $.get("backendSearch.php", {term: inputVal}).done(function (data) {
                        resultDropdown.html(data);
                    });
                } else {
                    resultDropdown.empty();
                }
            });

            $(document).on("click", ".result p", function () {
                $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
                $(this).parent(".result").empty();
            });
        });
    </script>
<?php
function navBarUser()
{
?>
<!DOCTYPE html>
<header>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CodeForFood</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="zaposleni.php">HOME</a></li>
                <li><a href="zaposleni.php">ZAPOSLENI</a></li>
                <li><a href="nedelja.php">NEDELJA</a></li>
                <li><a href="evidencija.php">EVIDENCIJA</a></li>
                <li><a href="kategorija.php">KATEGORIJA</a></li>
                <li><a href="evidencija_kategorija.php">EVIDENCIJA KATEGORIJA</a></li>
            </ul>

            <div class="nav navbar-nav navbar-right">
                <!--<div class="container-fluid"-->
                <?php
                if(isset($_SESSION['IDUSER'])){
                    echo '<form action="logout.php" method="POST">
								<button class="btn btn-primary" type="submit" name ="submit">Logout</button>
								</form>';
                }else{
                    echo '<ul class="nav navbar-nav navbar-right">
                            <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                            <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                         </ul>';
                } ?>
            </div>
        </div>
        </div>
    </nav>

</header>
<?php } ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    

</html>