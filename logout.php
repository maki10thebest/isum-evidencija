<?php
// Zapocni sesiju
session_start();

// Ponistavalje varijabli sesije
$_SESSION = array();

// Unistavanje sesije
session_destroy();

// Redirekcija na na login stranu
header("location: login.php");
exit;
?>
