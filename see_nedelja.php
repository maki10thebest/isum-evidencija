<?php
require_once 'navBar.php';
// Pokreni sesiju
session_start();
navBarUser();
if(!isset($_SESSION['USERNAME']) || empty($_SESSION['USERNAME'])){
    header("location: login.php");
    exit;
}

?>

    <!DOCTYPE html>
    <html>
    <head>
        <!-- BOOTSTRAP-->
        <title>NEDELJA</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style>
            .form-control{
                width: 50%;
            }
            .row {
                margin-left: 250px;
            }
            #myInput {
                background-image: url('/css/searchicon.png');
                background-position: 10px 10px;
                background-repeat: no-repeat;
                width: 100%;
                font-size: 16px;
                padding: 12px 20px 12px 40px;
                border: 1px solid #ddd;
                margin-bottom: 12px;
            }
        </style>
    </head>
<body>

<!-- DB connection-->
<?php
$mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));

if(isset($_GET['see'])) {
    $id = $_GET['see'];
    $result_see = $mysqli->query("SELECT zaposleni.IME, zaposleni.PREZIME, nedelja.RBNEDELJE, evidencija.VREMEDOLASKA, evidencija.VREMEODLASKA FROM evidencija INNER JOIN nedelja ON evidencija.IDNEDELJA = nedelja.IDNEDELJA
 INNER JOIN zaposleni ON zaposleni.IDZAPOSLENI = evidencija.IDZAPOSLENI WHERE nedelja.IDNEDELJA = $id") or die(mysqli_error($mysqli)); }?>


    <!--STAMPA-->
<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
    <div class="row justify-content-center">
        <table class="table" id="myTable">
            <thead>
            <tr>
                <th>Ime</th>
                <th>Redni broj nedelje</th>
                <th>Vreme dolaska</th>
                <th>Vreme odlaska</th>
                <th>Provedeno vreme</th>
                <th colspan="2"> Akcija</th>
            </tr>
            </thead>

            <?php
            while ($row = $result_see->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['IME']; echo ' '; echo $row['PREZIME']; ?></td>
                    <td><?php echo $row['RBNEDELJE']; ?></td>
                    <td><?php echo $row['VREMEDOLASKA']; ?></td>
                    <td><?php echo $row['VREMEODLASKA']; ?></td>
                    <?php $time = strtotime(date($row['VREMEODLASKA'])) - strtotime($row['VREMEDOLASKA']);?>
                    <td><?php echo $time/60;?></td>
                    <td></td>
                    <td>
                        <a href="nedelja.php?edit=<?php echo $row['IDNEDELJA']; ?>"
                           class="btn btn-info">Edit</a>
                        <a href="nedelja.php?see=<?php echo $row['IDNEDELJA'] ?>"
                           class="btn btn-warning">See</a>
                        <a href="process_nedelja.php?delete=<?php echo $row['IDNEDELJA'] ?>"
                           class="btn btn-danger">Delete</a>

                    </td>
                </tr>
            <?php endwhile; ?>
        </table>
    </div>

<script>
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
<!--KREIRA NOVOG ZAPOSLENOG-->
<div class="row justify-content-center">
    <form action="process_evidencija.php" method="POST">
        <input type="hidden" name="IDNEDELJA" value="<?php echo $id; ?>">
        <div class="form-group">
            <label>ZAPOSLENI</label>
            <select name="ime" class="form-control">
                <?php   $result2 = $mysqli->query("SELECT * FROM zaposleni") or die($mysqli->error);
                while ($row2 = $result2->fetch_assoc()): ?>
                    <option value="<?php echo $row2['IDZAPOSLENI']; ?>"><?php echo $row2['IME']; echo ' '; echo $row2['PREZIME']; ?></option>
                <?php endwhile; ?>
            </select>
        </div>
            <div class="form-group">
                <label>NEDELJA</label>
                <select name="nedelja" class="form-control">
                    <?php   $result3 = $mysqli->query("SELECT * FROM nedelja") or die($mysqli->error);
                    while ($row3 = $result3->fetch_assoc()): ?>
                        <option value="<?php echo $row3['IDNEDELJA']; ?>"><?php echo $row3['RBNEDELJE']; ?></option>
                    <?php endwhile; ?>
                </select>
            </div>
        <div class="form-group">
            <label>Vreme dolaska</label>
            <input type="datetime-local" name="od" class="form-control" value="<?php echo $od; ?>" placeholder="Datum od">
        </div>
        <div class="form-group">
            <label>Vreme odlaska</label>
            <input type="datetime-local" name="do" class="form-control" value="<?php echo $do; ?>" placeholder="Datum do">
        </div>
        <div class="form-group">
            <?php
           /* if($update == true):
                ?>
                <button type="submit" class="btn btn-info" name="update">Update</button>
            <?php else: */ ?>

                <button type="submit" name="save" class="btn btn-primary">Save</button>
            <?php// endif; ?>
        </div>
    </form>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
</body>
</html>
