<?php
require_once 'navBar.php';
// Pokreni sesiju
session_start();
navBarUser();
if(!isset($_SESSION['USERNAME']) || empty($_SESSION['USERNAME'])){
    header("location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html>
<head>
    <!-- BOOTSTRAP-->
    <title>NEDELJA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .form-control{
            width: 50%;
        }
    </style>
</head>
<body>

<!--ukljucuje CRUD-->
<?php require_once 'process_nedelja.php'; ?>

<?php
//PORUKA NAKON AKCIJA
if (isset($_SESSION['message'])): ?>

    <div class="alert alert-<?=$_SESSION['msg_type']?>">
        <?php
        echo $_SESSION['message'];
        unset ($_SESSION['message']);
        ?>
    </div>
<?php endif ?>
<!--Povezuje se na bazu-->
<div class="container">
    <?php
    $mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));

    //READ upit
    $result = $mysqli->query("SELECT * FROM nedelja") or die($mysqli->error);
    ?>
    <!--STAMPA-->
    <div class="row justify-content-center">
        <table class="table">
            <thead>
            <tr>
                <th>Redni broj nedelje</th>
                <th>Datum od</th>
                <th>Datum do</th>
                <th colspan="2"> Akcija</th>
            </tr>
            </thead>

            <?php
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['RBNEDELJE']; ?></td>
                    <td><?php echo $row['DATUMOD']; ?></td>
                    <td><?php echo $row['DATUMDO']; ?></td>
                    <td>
                        <a href="nedelja.php?edit=<?php echo $row['IDNEDELJA']; ?>"
                           class="btn btn-info">Edit</a>
                        <a href="see_nedelja.php?see=<?php echo $row['IDNEDELJA'] ?>"
                           class="btn btn-warning">See</a>
                        <a href="process_nedelja.php?delete=<?php echo $row['IDNEDELJA'] ?>"
                           class="btn btn-danger">Delete</a>

                    </td>

                </tr>
            <?php endwhile; ?>
        </table>
    </div>

    <?php
    function pre_r($array) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }

    ?>

    <!--KREIRA NOVOG ZAPOSLENOG-->
    <div class="row justify-content-center">
        <form action="process_nedelja.php" method="POST">
            <input type="hidden" name="IDNEDELJA" value="<?php echo $id; ?>">
            <div class="form-group">
                <label>Redni broj nedelje</label>
                <input type="number" name="rb" class="form-control" value="<?php echo $rb; ?>" placeholder="Redni broj">
            </div>
            <div class="form-group">
                <label>Datum od</label>
                <input type="date" name="od" class="form-control" value="<?php echo $od; ?>" placeholder="Datum od">
            </div>
            <div class="form-group">
                <label>Datum do</label>
                <input type="date" name="do" class="form-control" value="<?php echo $do; ?>" placeholder="Datum do">
            </div>
            <div class="form-group">
                <?php
                if($update == true):
                    ?>
                    <button type="submit" class="btn btn-info" name="update">Update</button>
                <?php else: ?>
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
                <?php endif; ?>
            </div>
        </form>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>