<?php
require_once 'navBar.php';
// Pokreni sesiju
session_start();
navBarUser();
if(!isset($_SESSION['USERNAME']) || empty($_SESSION['USERNAME'])){
    header("location: login.php");
    exit;
}

    // DB CONNECTION

$mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));
if(isset($_GET['see'])) {
    $id = $_GET['see'];
    $result_see = $mysqli->query("SELECT zaposleni.IME, zaposleni.PREZIME, evidencija.VREMEDOLASKA, evidencija.VREMEODLASKA, nedelja.RBNEDELJE, evidencija_kategorija.IDKATEGORIJA, kategorija.CENA
    FROM evidencija INNER JOIN zaposleni ON evidencija.IDZAPOSLENI = zaposleni.IDZAPOSLENI
        INNER JOIN nedelja ON evidencija.IDNEDELJA = nedelja.IDNEDELJA
        INNER JOIN evidencija_kategorija ON evidencija.IDZAPOSLENI = evidencija_kategorija.IDZAPOSLENI
        INNER JOIN kategorija ON evidencija_kategorija.IDKATEGORIJA = kategorija.IDKATEGORIJA
        WHERE zaposleni.IDZAPOSLENI = $id")  or die(mysqli_error($mysqli)); }?>

<!--STAMPA-->

<div class="row justify-content-center">
    <table class="table" id="myTable">
        <thead>
        <tr>
            <th>Ime</th>
            <th>Redni broj nedelje</th>
            <th>Vreme dolaska</th>
            <th>Vreme odlaska</th>
            <th>Provedeno vreme</th>
            <th>Ukupno vreme</th>
            <th>Zarada za ukupno vreme</th>
            <!--<th colspan="2"> Akcija</th>-->
        </tr>
        </thead>

        <?php
        $sum = 0;
        $money = 0;
        while ($row = $result_see->fetch_assoc()): ?>
            <tr>
                <td><?php echo $row['IME']; echo ' '; echo $row['PREZIME'];?></td>
                <td><?php echo $row['RBNEDELJE']; ?></td>
                <td><?php echo $row['VREMEDOLASKA']; ?></td>
                <td><?php echo $row['VREMEODLASKA']; ?></td>
                <?php $time = strtotime(date($row['VREMEODLASKA'])) - strtotime($row['VREMEDOLASKA']); ?>
                <td><?php echo $time/60; echo ' minutes'?></td>
                <!--Ukupno provedeno vreme za sve dane, po zaposlenom-->
                <?php $sum += $time; ?>
                <?php $money = $row['CENA'];?>

                <?php endwhile; ?>
                <td><?php echo $sum/60; echo ' minutes' ?></td>
                <td><?php echo $sum/60 * $money ; echo ' $'; ?></td>

            </tr>
    </table><br><br><br><br>

</div>
  <?php  $dataPoints = array(
    array("y" => $sum/60 * $money,"label" => "Trenutna zarada" )
    ); ?>

<!--GRAPH-->
    <!-- GRAPH-->
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!DOCTYPE html>
<html>
<head>
    <!-- BOOTSTRAP-->
    <title>NEDELJA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--Graph-->
    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: ""
                },
                axisY: {
                    title: "Skolarina",
                    maximum: 2000,

                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## $",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();
        }
    </script>
    <style>
        .form-control{
            width: 50%;
        }
        .row {
            margin-left: 250px;
        }

    </style>
</head>
<body>