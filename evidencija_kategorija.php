<?php
require_once 'navBar.php';
// Pokreni sesiju
session_start();
navBarUser();
if(!isset($_SESSION['USERNAME']) || empty($_SESSION['USERNAME'])){
    header("location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html>
<head>
    <!-- BOOTSTRAP-->
    <title>EVIDENCIJA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .form-control{
            width: 50%;
        }
    </style>
</head>
<body>

<!--ukljucuje CRUD-->

<?php require_once 'process_evidencija.php'; ?>

<?php
//PORUKA NAKON AKCIJA
if (isset($_SESSION['message'])): ?>

    <div class="alert alert-<?=$_SESSION['msg_type']?>">
        <?php
        echo $_SESSION['message'];
        unset ($_SESSION['message']);
        ?>
    </div>
<?php endif ?>
<!--Povezuje se na bazu-->
<div class="container">
    <?php
    $mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));

    //READ upit
    // SELECT  Customers.CustomerName
    //FROM Customers
    //INNER JOIN Orders
    //ON Orders.CustomerID=Customers.CustomerID;
    $result = $mysqli->query("SELECT evidencija_kategorija.IDKATEGORIJA, evidencija_kategorija.DATUMULASKA, zaposleni.IME, zaposleni.PREZIME FROM evidencija_kategorija INNER JOIN zaposleni ON evidencija_kategorija.IDZAPOSLENI = zaposleni.IDZAPOSLENI") or die($mysqli->error);
    ?>
    <!--STAMPA-->
    <div class="row justify-content-center">
        <table class="table">
            <thead>
            <tr>
                <th>Kategorija</th>
                <th>Datum ulaska</th>
                <th>Zaposleni</th>
                <th colspan="2"> Akcija</th>
            </tr>
            </thead>

            <?php
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['IDKATEGORIJA']; ?></td>
                    <td><?php echo $row['DATUMULASKA']; ?></td>
                    <td><?php echo $row['IME']; echo ' '; echo $row['PREZIME']; ?></td>
                    <td>
                        <a href="evidencija_kategorija.php?edit=<?php echo $row['IDEVIDKATEG']; ?>"
                           class="btn btn-info">Edit</a>
                        <a href="process_evidencija_kategorija.php?delete=<?php echo $row['IDEVIDKATEG'] ?>"
                           class="btn btn-danger">Delete</a>
                    </td>

                </tr>
            <?php endwhile; ?>
        </table>
    </div>

    <?php
    function pre_r($array) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }

    ?>

    <!--KREIRA NOVOG ZAPOSLENOG-->
    <div class="row justify-content-center">
        <form action="process_evidencija_kategorija.php" method="POST">
            <input type="hidden" name="IDEVIDKATEG" value="<?php echo $id; ?>">
            <div class="form-group">
                <label>KATEGORIJA</label>
                <select name="kategorija" class="form-control">
                    <?php   $result2 = $mysqli->query("SELECT * FROM kategorija") or die($mysqli->error);
                    while ($row2 = $result2->fetch_assoc()): ?>
                        <option value="<?php echo $row2['IDKATEGORIJA']; ?>"><?php echo $row2['NAZIVKATEGORIJE'];?></option>
                    <?php endwhile; ?>
                </select>
            </div>
            <div class="form-group">
                <label>ZAPOSLENI</label>
                <select name="zaposleni" class="form-control">
                    <?php   $result3 = $mysqli->query("SELECT * FROM zaposleni") or die($mysqli->error);
                    while ($row3 = $result3->fetch_assoc()): ?>
                        <option value="<?php echo $row3['IDZAPOSLENI']; ?>"><?php echo $row3['IME']; echo ' '; echo $row3['PREZIME'];?></option>
                    <?php endwhile; ?>
                </select>
            </div>
            <div class="form-group">
                <label>DATUM</label>
                <input type="date" name="datum" class="form-control" value="<?php echo $datum; ?>" placeholder="Datum">
            </div>
            <div class="form-group">
                <?php
                if($update == true):
                    ?>
                    <button type="submit" class="btn btn-info" name="update">Update</button>
                <?php else: ?>
                    <button type="submit" name="save" class="btn btn-primary">Save</button>
                <?php endif; ?>
            </div>
        </form>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>