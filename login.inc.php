
<?php

session_start();

if (isset($_POST['submit'])) {

    $mysqli = new mysqli('localhost', 'root', '', 'vladabaza') or die(mysqli_error($mysqli));

    $uid = mysqli_real_escape_string($mysqli, $_POST['USERNAME']);
    $pwd = mysqli_real_escape_string($mysqli, $_POST['PASSWORD']);

    //Error handlers
    //Check if inputs are empty
    if (empty($uid) || empty($pwd)) {
        header("Location: ../login.php?login=empty");
        exit();
    } else {
        $sql = "SELECT * FROM user WHERE USERNAME='$uid' ";
        $result = mysqli_query($mysqli, $sql);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck < 1) {
            header("Location: ../login.php?login=error");
            exit();
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                //De-hashing the password
                $hashedPwdCheck = password_verify($pwd, $row['PASSWORD']);
                if ($hashedPwdCheck == false) {
                    header("Location: ../login.php?login=error");
                    exit();
                } elseif ($hashedPwdCheck == true) {
                    //Log in the user here
                    $_SESSION['IDUSER'] = $row['IDUSER'];
                    $_SESSION['EMAIL'] = $row['EMAIL'];
                    $_SESSION['USERNAME'] = $row['USERNAME'];
                    header("Location: http://localhost/ISUMEvidencija/ISUM%20evidencija/zaposleni.php?"); //http://localhost/ISUMEvidencija/ISUM%20evidencija/login.php
                    exit();
                }
            }
        }
    }
} else {
    header("Location: ../login.php?login=error");
    exit();
}

